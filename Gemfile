RAILS_ENV = ENV['RAILS_ENV'] || `cat .rails-env 2>/dev/null`.to_s.split.first || 'development'

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# ruby '2.6.3'

gem 'bootsnap', '>= 1.1.0', require: false

gem 'rails', '6.0.0'
gem 'graphql'

gem 'irb'
gem 'puma'

gem 'webpacker'

gem 'pg', '~> 1.0'
gem 'scenic'

gem 'hiredis'
gem 'redis'

# gem 'activerecord-postgis-adapter'
gem 'bcrypt'
gem 'kaminari'

gem 'rack-attack'
gem 'rack-cors'

gem 'faraday'

gem 'browser'

gem 'csv'

gem 'whenever'
# gem 'zhong'

gem 'sidekiq'

gem 'andpush'
gem 'discordrb-webhooks'
gem 'slack-notifier'
# gem 'telegram-bot'

gem 'jbuilder'
gem 'rails-i18n'

gem 'faker'
gem 'forgery'

gem 'request_store_rails'

gem 'attr_encrypted'
gem 'audited'
gem 'lockbox'

# gem 'jemalloc'
gem 'tzinfo-data'

gem 'phonelib'

group :development do
  # Use Capistrano for deployment
  gem 'capistrano'
  gem 'capistrano-bundler'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano-safe-deploy-to'
  # gem 'capistrano-sidekiq'
  gem 'capistrano-slackify'
  gem 'capistrano3-puma'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # https://github.com/Arkweid/lefthook
  gem 'lefthook'
end

group :development, :test do
  gem 'brakeman'
  gem 'bundle-audit'
  gem 'database_cleaner'
  gem 'rspec'
  # https://github.com/rspec/rspec-rails
  gem 'rspec-rails'
  gem 'rubocop'
  gem 'rubocop-gitlab-security'
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'rubocop-rspec'

  gem 'derailed_benchmarks'
  gem 'memory_profiler'

  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

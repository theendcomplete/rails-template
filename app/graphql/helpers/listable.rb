module Helpers::Listable
  def listable(name, type, override_options: {})
    default_field_options = { type: type, null: false }
    field_options = default_field_options.merge(override_options)
    field(name, field_options) do
      argument :options, InputObjects::IndexOptions, required: false
      yield if block_given?
    end
  end
end

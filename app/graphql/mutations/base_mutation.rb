module Mutations
  class BaseMutation < GraphQL::Schema::Mutation
    include Helpers::QueryReducer
    include Helpers::Authorize
  end
end

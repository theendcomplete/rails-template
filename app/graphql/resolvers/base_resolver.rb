module Resolvers
  class BaseResolver < GraphQL::Schema::Resolver
    argument_class Arguments::BaseArgument
    include Helpers::QueryReducer
    include Helpers::Authorize
    extend Helpers::Listable
  end
end

module Types
  class BaseObject < GraphQL::Schema::Object
    include Helpers::QueryReducer
    include Helpers::Authorize
    extend Helpers::Listable
  end
end

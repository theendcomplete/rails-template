module Types
  class MutationType < Types::BaseObject
    field :create_auth, mutation: Mutations::Users::CreateAuthMutation

    field :create_user, mutation: Mutations::Users::CreateUserMutation
    field :update_user, mutation: Mutations::Users::UpdateUserMutation
    field :destroy_user, mutation: Mutations::Users::DestroyUserMutation

    field :update_profile, mutation: Mutations::Users::UpdateProfileMutation
    field :destroy_profile, mutation: Mutations::Users::DestroyProfileMutation
  end
end

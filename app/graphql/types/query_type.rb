module Types
  class QueryType < Types::BaseObject
    field :profile, Types::Users::UserType, null: false
    def profile
      authorize_user
      User.find(context[:user].id)
    end

    listable :users, Types::Users::UsersType
    def users(options:)
      authorize_user [UserRole::Admin]
      results = User.accessible(context[:user])
      reduce_query(results, options)
    end
  end
end

class UserRole < ApplicationRecordEnum
  codes %w[admin observer sales_manager agency_manager agency_supervisor agency_demonstrator]
end

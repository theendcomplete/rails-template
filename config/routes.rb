Rails.application.routes.draw do
  post '/graphql', to: 'graphql#execute'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'application#root'
  get '/404', to: 'application#handle_error_404'
  get '/500', to: 'application#handle_error_500'

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      namespace :users do
        resource :auth
        resources :actions, param: :token, only: %i[show update create]
        resource :profile
      end
      resources :users do
        post :password
        collection do
          post :import
        end
      end
    end
  end
end

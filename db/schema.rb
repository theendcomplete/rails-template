# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_23_095955) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "agencies", force: :cascade do |t|
    t.string "title", limit: 64, null: false
    t.string "status_code", limit: 32, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_agencies_on_title", unique: true
  end

  create_table "agency_districts", force: :cascade do |t|
    t.bigint "agency_id", null: false
    t.bigint "district_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agency_id", "district_id"], name: "index_agency_district_unique", unique: true
    t.index ["agency_id"], name: "index_agency_districts_on_agency_id"
    t.index ["district_id"], name: "index_agency_districts_on_district_id"
  end

  create_table "agency_teams", force: :cascade do |t|
    t.string "title", limit: 64, null: false
    t.string "status_code", limit: 32, null: false
    t.bigint "agency_id", null: false
    t.bigint "district_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agency_id"], name: "index_agency_teams_on_agency_id"
    t.index ["district_id"], name: "index_agency_teams_on_district_id"
    t.index ["title", "agency_id"], name: "index_agency_team_unique", unique: true
  end

  create_table "course_assignment_test_sessions", force: :cascade do |t|
    t.bigint "course_assignment_id", null: false
    t.datetime "expires_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.decimal "result", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_assignment_id"], name: "index_course_assignment_test_sessions_on_course_assignment_id"
  end

  create_table "course_assignments", force: :cascade do |t|
    t.bigint "educational_course_id", null: false
    t.bigint "user_id", null: false
    t.string "status_code", limit: 64, null: false
    t.bigint "author_id"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_course_assignments_on_author_id"
    t.index ["educational_course_id"], name: "index_course_assignments_on_educational_course_id"
    t.index ["status_code"], name: "index_course_assignments_on_status_code"
    t.index ["user_id"], name: "index_course_assignments_on_user_id"
  end

  create_table "course_recommendation_actions", force: :cascade do |t|
    t.bigint "course_recommendation_id"
    t.bigint "user_id"
    t.string "type_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "comment"
    t.index ["course_recommendation_id"], name: "index_course_recommendation_actions_on_course_recommendation_id"
    t.index ["user_id"], name: "index_course_recommendation_actions_on_user_id"
  end

  create_table "course_recommendation_agencies", force: :cascade do |t|
    t.bigint "course_recommendation_id", null: false
    t.bigint "agency_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agency_id"], name: "index_course_recommendation_agencies_on_agency_id"
    t.index ["course_recommendation_id"], name: "idx_course_recommendation_to_agencies"
  end

  create_table "course_recommendation_districts", force: :cascade do |t|
    t.bigint "course_recommendation_id", null: false
    t.bigint "district_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_recommendation_id"], name: "idx_course_recommendation_to_districts"
    t.index ["district_id"], name: "index_course_recommendation_districts_on_district_id"
  end

  create_table "course_recommendation_users", force: :cascade do |t|
    t.bigint "course_recommendation_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status_code"
    t.index ["course_recommendation_id"], name: "idx_course_recommendation_to_users"
    t.index ["user_id"], name: "index_course_recommendation_users_on_user_id"
  end

  create_table "course_recommendations", force: :cascade do |t|
    t.bigint "educational_course_id", null: false
    t.string "comment"
    t.bigint "author_id", null: false
    t.string "user_role_codes", array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_course_recommendations_on_author_id"
    t.index ["educational_course_id"], name: "index_course_recommendations_on_educational_course_id"
    t.index ["user_role_codes"], name: "idx_course_recommendation_to_user_role_codes", using: :gin
  end

  create_table "districts", force: :cascade do |t|
    t.string "title", limit: 64, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_districts_on_title", unique: true
  end

  create_table "educational_courses", force: :cascade do |t|
    t.bigint "educational_material_id", null: false
    t.bigint "test_id"
    t.string "title", limit: 100, null: false
    t.string "description", limit: 255
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_educational_courses_on_author_id"
    t.index ["educational_material_id"], name: "index_educational_courses_on_educational_material_id"
    t.index ["test_id"], name: "index_educational_courses_on_test_id"
  end

  create_table "educational_material_tags", force: :cascade do |t|
    t.bigint "educational_material_id", null: false
    t.bigint "tag_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["educational_material_id", "tag_id"], name: "index_educational_material_tags_uniqueness", unique: true
    t.index ["educational_material_id"], name: "index_educational_material_tags_on_educational_material_id"
    t.index ["tag_id"], name: "index_educational_material_tags_on_tag_id"
  end

  create_table "educational_materials", force: :cascade do |t|
    t.string "title", limit: 100, null: false
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "public", null: false
    t.index ["author_id"], name: "index_educational_materials_on_author_id"
  end

  create_table "question_answers", force: :cascade do |t|
    t.string "text", limit: 100, null: false
    t.bigint "question_id", null: false
    t.boolean "correct", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_question_answers_on_question_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "type_code", limit: 64, null: false
    t.jsonb "statement", default: {}, null: false
    t.jsonb "answer"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_questions_on_author_id"
    t.index ["type_code"], name: "index_questions_on_type_code"
  end

  create_table "server_errors", force: :cascade do |t|
    t.text "message"
    t.text "backtrace"
    t.string "request_token"
    t.string "request_remote_ip"
    t.string "request_method"
    t.string "request_original_url"
    t.string "request_params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "extra"
  end

  create_table "settings", force: :cascade do |t|
    t.string "key", null: false
    t.jsonb "value"
    t.string "datatype", limit: 16, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "index_settings_on_key", unique: true
    t.index ["value"], name: "index_settings_on_value"
  end

  create_table "tags", force: :cascade do |t|
    t.string "title", limit: 32, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["title"], name: "index_tags_on_title", unique: true
  end

  create_table "test_cards", force: :cascade do |t|
    t.bigint "test_id", null: false
    t.bigint "question_id", null: false
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_test_cards_on_author_id"
    t.index ["question_id"], name: "index_test_cards_on_question_id"
    t.index ["test_id"], name: "index_test_cards_on_test_id"
  end

  create_table "test_results", force: :cascade do |t|
    t.bigint "test_id", null: false
    t.bigint "user_id", null: false
    t.bigint "question_answer_id", null: false
    t.bigint "course_assignment_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_assignment_id"], name: "index_test_results_on_course_assignment_id"
    t.index ["question_answer_id"], name: "index_test_results_on_question_answer_id"
    t.index ["test_id"], name: "index_test_results_on_test_id"
    t.index ["user_id"], name: "index_test_results_on_user_id"
  end

  create_table "test_session_question_answers", force: :cascade do |t|
    t.bigint "course_assignment_test_session_id", null: false
    t.bigint "question_id", null: false
    t.decimal "result", null: false
    t.jsonb "user_answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_assignment_test_session_id"], name: "index_course_assignment_test_session"
    t.index ["question_id"], name: "index_test_session_question_answers_on_question_id"
  end

  create_table "tests", force: :cascade do |t|
    t.string "title", limit: 100, null: false
    t.string "type_code", limit: 64, null: false
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_tests_on_author_id"
  end

  create_table "user_action_tokens", force: :cascade do |t|
    t.string "token", limit: 256, null: false
    t.string "type_code", limit: 64, null: false
    t.bigint "user_id", null: false
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_action_tokens_on_user_id"
  end

  create_table "user_agencies", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "agency_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agency_id"], name: "index_user_agencies_on_agency_id"
    t.index ["user_id", "agency_id"], name: "index_user_agency_unique", unique: true
    t.index ["user_id"], name: "index_user_agencies_on_user_id"
  end

  create_table "user_agency_teams", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "agency_team_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["agency_team_id"], name: "index_user_agency_teams_on_agency_team_id"
    t.index ["user_id", "agency_team_id"], name: "index_user_agency_team_unique", unique: true
    t.index ["user_id"], name: "index_user_agency_teams_on_user_id"
  end

  create_table "user_districts", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "district_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["district_id"], name: "index_user_districts_on_district_id"
    t.index ["user_id", "district_id"], name: "index_user_district_unique", unique: true
    t.index ["user_id"], name: "index_user_districts_on_user_id"
  end

  create_table "user_tokens", id: false, force: :cascade do |t|
    t.string "token", null: false
    t.bigint "user_id"
    t.string "http_remote_addr", null: false
    t.string "http_user_agent", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["token", "user_id"], name: "index_user_tokens_on_token_and_user_id"
    t.index ["token"], name: "index_user_tokens_on_token", unique: true
    t.index ["user_id"], name: "index_user_tokens_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login", limit: 48, null: false
    t.string "email", limit: 48, null: false
    t.string "password_digest", limit: 1024, null: false
    t.string "first_name", limit: 48
    t.string "last_name", limit: 48
    t.string "status_code", limit: 64, null: false
    t.string "role_code", limit: 64, null: false
    t.string "demo_id", limit: 64
    t.datetime "password_updated_at"
    t.datetime "archived_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "auth_attempts", default: 0
    t.datetime "auth_blocked_until"
    t.index ["demo_id"], name: "index_users_on_demo_id", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["login"], name: "index_users_on_login", unique: true
    t.index ["role_code"], name: "index_users_on_role_code"
    t.index ["status_code"], name: "index_users_on_status_code"
  end

  add_foreign_key "agency_districts", "agencies"
  add_foreign_key "agency_districts", "districts"
  add_foreign_key "agency_teams", "agencies"
  add_foreign_key "agency_teams", "districts"
  add_foreign_key "course_assignment_test_sessions", "course_assignments"
  add_foreign_key "course_assignments", "educational_courses"
  add_foreign_key "course_assignments", "users"
  add_foreign_key "course_assignments", "users", column: "author_id"
  add_foreign_key "course_recommendation_actions", "course_recommendations"
  add_foreign_key "course_recommendation_actions", "users"
  add_foreign_key "course_recommendation_agencies", "agencies"
  add_foreign_key "course_recommendation_agencies", "course_recommendations"
  add_foreign_key "course_recommendation_districts", "course_recommendations"
  add_foreign_key "course_recommendation_districts", "districts"
  add_foreign_key "course_recommendation_users", "course_recommendations"
  add_foreign_key "course_recommendation_users", "users"
  add_foreign_key "course_recommendations", "educational_courses"
  add_foreign_key "course_recommendations", "users", column: "author_id"
  add_foreign_key "educational_courses", "educational_materials"
  add_foreign_key "educational_courses", "tests"
  add_foreign_key "educational_courses", "users", column: "author_id"
  add_foreign_key "educational_material_tags", "educational_materials"
  add_foreign_key "educational_material_tags", "tags"
  add_foreign_key "educational_materials", "users", column: "author_id"
  add_foreign_key "question_answers", "questions"
  add_foreign_key "questions", "users", column: "author_id"
  add_foreign_key "test_cards", "questions"
  add_foreign_key "test_cards", "tests"
  add_foreign_key "test_cards", "users", column: "author_id"
  add_foreign_key "test_results", "course_assignments"
  add_foreign_key "test_results", "question_answers"
  add_foreign_key "test_results", "tests"
  add_foreign_key "test_results", "users"
  add_foreign_key "test_session_question_answers", "course_assignment_test_sessions"
  add_foreign_key "test_session_question_answers", "questions"
  add_foreign_key "tests", "users", column: "author_id"
  add_foreign_key "user_action_tokens", "users"
  add_foreign_key "user_agencies", "agencies"
  add_foreign_key "user_agencies", "users"
  add_foreign_key "user_agency_teams", "agency_teams"
  add_foreign_key "user_agency_teams", "users"
  add_foreign_key "user_districts", "districts"
  add_foreign_key "user_districts", "users"
  add_foreign_key "user_tokens", "users"
end
